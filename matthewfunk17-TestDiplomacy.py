from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Paris Move Barcelona\nB Barcelona Move London\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Paris Move Boston\nB Barcelona Move Boston\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Boston\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Rome Hold\nB NewYork Move Rome\nC London Move Rome\nD Madrid Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Rome\nC [dead]\nD Madrid\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()                 # pragma: no cover
