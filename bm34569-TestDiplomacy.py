# TestDiplomacy.py

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
    """
    # read
    def test_read(self):
        s = 'A Madrid Hold\n'
        a = diplomacy_read(s)
        self.assertEqual(list(a), ['A', 'Madrid', 'Hold'])
        
    def test_read2(self):
        s = 'B London Move Madrid\n'
        a = diplomacy_read(s)
        self.assertEqual(list(a), ['B', 'London', 'Move', 'Madrid'])
        
    def test_read3(self):
        s = 'C Austin Support A\n'
        a = diplomacy_read(s)
        self.assertEqual(list(a), ['C', 'Austin', 'Support', 'A'])
        """
    # eval
    def test_solve(self):
        r = StringIO("A Austin Hold\nB Boston Move Austin\nC Paris Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Austin\nB [dead]\nC Paris\n")
        #
    def test_solve2(self):
        r = StringIO("A Arlington Hold\nB Dallas Hold\nC Houston Support B\nD Paris Move Arlington\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB Dallas\nC Houston\nD [dead]\n")
        #
    def test_solve3(self):
        r = StringIO("A Austin Hold\nB Breckenridge Hold\nC Carington Move Breckenridge\nD Winnipeg Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Austin\nB Breckenridge\nC [dead]\nD Winnipeg\n")

    def test_solve4(self):
        r = StringIO("A Arlington Hold\nB Boston Move Arlington\nC Carlington Support A\nD Massachusetts Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Arlington\nB [dead]\nC Carlington\nD Massachusetts\n")
        #
if __name__ == "__main__":
    main()